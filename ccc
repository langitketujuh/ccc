#!/usr/bin/env bash

VERSION="v0.0.4"
LICENSE="GPL-3.0-or-later"

# icc profiles
DIR_PROFILES="/usr/share/color/icc/profiles"
GSCRIPT_CMYK="$DIR_PROFILES/ghostscript/default_cmyk.icc"
KRITA_CMYK="$DIR_PROFILES/krita/cmyk.icm"
SCRIBUS_CMYK="$DIR_PROFILES/scribus/ISOcoated_v2_300_bas.icc"
SCRIBUS_CMYK_FOGRA="$DIR_PROFILES/scribus/GenericCMYK.icm"

# usage
usage() {
echo -e "\033[1;36mCCC (CMYK Color Converter) $VERSION\033[0m\n"
    cat <<- EOF
Usage   :   ccc -p [1-4] -i [input.(png,jpg,pdf)] -o [output.(jpg,tiff,pdf)]
Option  :   -p  color profile (default=1).
                [1] Ghostscript (Artifex CMYK SWOP Profile).
                [2] Krita       (Chemical proof).
                [3] Scribus     (ISO Coated v2 300% basICColor).
                [4] Scribus     (Fogra27L CMYK Coated Press).
            -i  input file. support png, jpg and pdf format.
            -o  output file. support png, tiff and pdf format.
            -h  show this help.

EOF
}

# parameters
while getopts "c:i:o:p:h" opt; do
    case $opt in
        c) COLOR="$OPTARG";;
        i) INPUT="$OPTARG";;
        o) OUTPUT="$OPTARG";;
        p) PROFILE="$OPTARG";;
        h) usage;;
        *) exit;;
    esac
done
shift $((OPTIND - 1))

# set defaults value
: ${PROFILE:=1} # ghostscript

# variable file and extension
IN_FILENAME=$(basename -- "$INPUT")
IN_EX="${IN_FILENAME##*.}"
IN_NAME="${IN_FILENAME%.*}"

OUT_FILENAME=$(basename -- "$OUTPUT")
OUT_EX="${OUT_FILENAME##*.}"
OUT_NAME="${OUT_FILENAME%.*}"

# check if empty or not support file
if [ -z "$INPUT" ]; then
    echo "Empty the input file (-i)."
    exit;
fi

if [ -z "$OUTPUT" ]; then
    echo "Empty the output file (-o)."
    exit;
fi

if [[ "$IN_EX" == "pdf" && ! "$OUT_EX" == "pdf" ]]; then
    echo "If the input is pdf, use pdf too for output."
    exit;
fi

if ! [[ "$IN_EX" == "png" || "$IN_EX" == "jpg" || "$IN_EX" == "pdf" ]]; then
    echo "The input file not support. Use png, jpg or pdf."
    exit;
fi

if ! [[ "$OUT_EX" == "jpg" || "$OUT_EX" == "tiff" || "$OUT_EX" == "pdf" ]]; then
    echo "The output file not support. Use jpg, tiff or pdf."
    exit;
fi

# variable icc profiles
if [ $PROFILE = 1 ]; then
    PROFILE=$GSCRIPT_CMYK
elif [ $PROFILE = 2 ]; then
    PROFILE=$KRITA_CMYK
elif [ $PROFILE = 3 ]; then
    PROFILE=$SCRIBUS_CMYK
elif [ $PROFILE = 4 ]; then
    PROFILE=$SCRIBUS_CMYK_FOGRA
fi

# main
if [ "$IN_EX" = "pdf" ]; then
    OUT_EX=$IN_EX
    gs -q -dSAFER -dBATCH -dNOPAUSE -dNOCACHE -sDEVICE=pdfwrite \
    -sColorConversionStrategy=CMYK -dProcessColorModel=/DeviceCMYK \
    -sOutputFile="$OUT_NAME.$OUT_EX" "$INPUT"
elif [[ "$IN_EX" = "png" || "$IN_EX" = "jpg" ]]; then
    convert "$INPUT" \
    -depth 8 -flatten -quality 100 \
    -colorspace CMYK -background white \
    -profile $PROFILE "$OUT_NAME.$OUT_EX"
fi
