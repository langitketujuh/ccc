PREFIX = /usr

all:
	@echo Run \'make install\' to install ccc.

install:
	@mkdir -p $(DESTDIR)$(PREFIX)/bin
	@mkdir -p $(DESTDIR)$(PREFIX)/share/color/icc/
	@cp -p ccc $(DESTDIR)$(PREFIX)/bin/ccc
	@cp -pr profiles $(DESTDIR)$(PREFIX)/share/color/icc/
	@chmod 755 $(DESTDIR)$(PREFIX)/bin/ccc

uninstall:
	@rm -rf $(DESTDIR)$(PREFIX)/bin/ccc
	@rm -rf $(DESTDIR)$(PREFIX)/share/color/icc/profiles/ghostscript/default_cmyk.icc
	@rm -rf $(DESTDIR)$(PREFIX)/share/color/icc/profiles/krita/README
	@rm -rf $(DESTDIR)$(PREFIX)/share/color/icc/profiles/krita/cmyk.icm
	@rm -rf $(DESTDIR)$(PREFIX)/share/color/icc/profiles/scribus/GenericCMYK.*
	@rm -rf $(DESTDIR)$(PREFIX)/share/color/icc/profiles/scribus/ISOcoated_v2_300_bas.*
