## Install

### LangitKetujuh OS

  ```sh
  get ccc
  ```

### Other GNU/Linux

  ```sh
  git clone https://gitlab.com/langitketujuh/ccc
  cd ccc
  sudo make install
  ```

## Help

```
~ ccc -h

Usage   :   ccc -p [1-4] -i [input.(png,jpg,pdf)] -o [output.(jpg,tiff,pdf)]
Option  :   -p  color profile (default=1).
                [1] Ghostscript (Artifex CMYK SWOP Profile).
                [2] Krita       (Chemical proof).
                [3] Scribus     (ISO Coated v2 300% basICColor).
                [4] Scribus     (Fogra27L CMYK Coated Press).
            -i  input file. support png, jpg and pdf format.
            -o  output file. support png, tiff and pdf format.
            -h  show this help.

Empty the input file (-i).

```
